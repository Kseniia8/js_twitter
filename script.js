class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
    }

    createCard() {
        const card = document.createElement('div');
        card.className = 'card';
        card.innerHTML = `
            <h3>${this.post.title}</h3>
            <p>${this.post.body}</p>
            <p class="author"><strong>${this.user.name} ${this.user.surname}</strong> (${this.user.email})</p>
            <button class="delete-button">Delete</button>
            <button class="edit-button">Edit</button>
        `;

        card.querySelector('.delete-button').addEventListener('click', () => this.deleteCard(card));
        card.querySelector('.edit-button').addEventListener('click', () => this.editCard(card));

        return card;
    }

    async deleteCard(cardElement) {
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE',
        });

        if (response.ok) {
            cardElement.remove();
        }
    }

    editCard(cardElement) {
        const modal = document.getElementById('modal');
        const titleInput = document.getElementById('title');
        const bodyInput = document.getElementById('body');

        modal.style.display = "block";
        titleInput.value = this.post.title;
        bodyInput.value = this.post.body;

        const form = document.getElementById('postForm');
        form.onsubmit = async (event) => {
            event.preventDefault();
            const updatedPost = {
                title: titleInput.value,
                body: bodyInput.value
            };

            const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedPost),
            });

            if (response.ok) {
                this.post.title = updatedPost.title;
                this.post.body = updatedPost.body;
                cardElement.querySelector('h3').textContent = this.post.title;
                cardElement.querySelector('p').textContent = this.post.body;
                modal.style.display = "none";
            }
        };
    }
}

async function fetchData(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

async function init() {
    const users = await fetchData('https://ajax.test-danit.com/api/json/users');
    const posts = await fetchData('https://ajax.test-danit.com/api/json/posts');

    const postsContainer = document.getElementById('posts');
    postsContainer.innerHTML = '';

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        const card = new Card(post, user);
        postsContainer.appendChild(card.createCard());
    });

    document.getElementById('addPostButton').addEventListener('click', () => {
        const modal = document.getElementById('modal');
        modal.style.display = "block";
    });

    document.getElementsByClassName('close')[0].addEventListener('click', () => {
        const modal = document.getElementById('modal');
        modal.style.display = "none";
    });

    window.addEventListener('click', (event) => {
        const modal = document.getElementById('modal');
        if (event.target === modal) {
            modal.style.display = "none";
        }
    });

    document.getElementById('postForm').addEventListener('submit', async (event) => {
        event.preventDefault();
        const title = document.getElementById('title').value;
        const body = document.getElementById('body').value;
        const newPost = {
            title,
            body,
            userId: 1
        };

        const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newPost),
        });

        const postData = await response.json();
        const user = users.find(user => user.id === 1);
        const card = new Card(postData, user);
        postsContainer.insertBefore(card.createCard(), postsContainer.firstChild);

        document.getElementById('modal').style.display = "none";
    });
}

document.addEventListener('DOMContentLoaded', init);
